<?php

namespace Drupal\hfc_elevate_api\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An example controller.
 */
class ExampleController extends ControllerBase {

  public function content() {
    $content = \Drupal::service("hfc_elevate_api")->webCommonData();

    $getValues = function($class) {
      return (
        '<ul><li>ID: '.$class['id'].'</li>'.
        '<li>COURSE CODE: '.$class['courseCode'].'</li>'.
        '<li>COURSE DESCRIPTION: '.$class['courseDescription'].'</li>'.
        '<li>COURSE YEAR: '.$class['courseYear'].'</li>'.
        '<li>START DATE: '.$class['startDate'].'</li>'.
        '<li>SESSION: '.$class['session'].'</li>'.
        '<li>COURSE FEE: '.$class['courseFee'].'</li>'.
        '<li>LOCATION CODE: '.$class['locationCode'].'</li>'.
        '<li>LOCATION DESCRIPTION: '.$class['locationDescription'].'</li>'.
        '<li>INSTANCE STATUS CODE: '.$class['instanceStatusCode'].'</li>'.
        '<li>INSTANCE STATUS DESCRIPTION: '.$class['instanceStatusDescription'].'</li></ul>'
      );
    };
    $classes = array_map($getValues, $content);

    $build['class_data'] = [
      '#markup' => '<pre>'.implode($classes).'</pre>'
    ];
    return $build;
  }
}
