<?php

namespace Drupal\hfc_elevate_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AdminSettingsForm.
 *
 * @package Drupal\hfc_elevate_api\Form
 */
class AdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'hfc_elevate_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hfc_elevate_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('hfc_elevate_api.settings');

    $form['web_common'] = [
      '#title' => 'Elevate API Web Common',
      '#type' => 'textfield',
      '#default_value' => $config->get('web_common'),
      '#description' => $this->t('The URL of the connection. Do not include a trailing slash.'),
      '#required' => TRUE,
    ];

    $form['course_structure'] = [
      '#title' => 'Elevate API Web Course Structure',
      '#type' => 'textfield',
      '#default_value' => $config->get('course_structure'),
      '#description' => $this->t('The URL of the connection. Do not include a trailing slash.'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('hfc_elevate_api.settings')
      ->set('web_common', $form_state->getValue('web_common'))
      ->set('course_structure', $form_state->getValue('course_structure'))
      ->save();
  }
}
