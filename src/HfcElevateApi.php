<?php

namespace Drupal\hfc_elevate_api;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Template\Attribute;
use GuzzleHttp\RequestOptions;

/**
 * Defines the Elevate API.
 */
class HfcElevateApi implements HfcElevateApiInterface {

  /**
   * {@inheritdoc}
   */
  public static function webCommonData() {
    $config = \Drupal::config('hfc_elevate_api.settings');
    if ($common = $config->get('web_common')) {
      $xml = self::request($common);
      $json = json_encode($xml->instances->courseInstance);
      $instances = json_decode($json, TRUE);
      array_shift($instances);

      $getCourseData = function($course) {
        $courseData = $course['@attributes'];
        return $courseData;
      };
      $courses = array_map($getCourseData, $instances);
      return $courses;
    }
    else {
      drupal_set_message(t('Elevate API web common connection not set. Please configure <a href="/admin/config/hfc/hfc-elevate-api">API settings</a>.'));
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function courseStructureData($course) {
    $config = \Drupal::config('hfc_elevate_api.settings');
    if ($url = $url->get('course_structure')) {
      // @todo: Figure out how to add the course to the request URL.
      // return self::request($course);
      die('This does not work yet.');
    }
    else {
      drupal_set_message(t('Elevate API course structure connection not set. Please configure <a href="/admin/config/hfc/hfc-elevate-api">API settings</a>.'));
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function request($url) {
    $response = \Drupal::httpClient()->request('GET', $url, [RequestOptions::TIMEOUT => 30]);
    if ($response->getStatusCode() == 200) {
      if (!empty($response->getBody())) {
        return simplexml_load_string($response->getBody());
      }
    }
    else {
      drupal_set_message(t('An error occurred retrieving Elevate data. Please try again later.'));
    }
  }


}
