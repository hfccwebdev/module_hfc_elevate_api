<?php

namespace Drupal\hfc_elevate_api;

/**
 * Defines the Elevate API Interface.
 */
interface HfcElevateApiInterface {

  /**
   * Query the API.
   */
  public static function webCommonData();

  /**
   * Query the API.
   *
   * @param string $course
   *   URL for course structure data
   */
  public static function courseStructureData($course);
}
